public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Person person1 = new Person("Quan", "Quận 6");
        Person person2 = new Person("Boi", "Quận Bình Tân");
        System.out.println(person1 + "," + person2);

        Student student1 = new Student("Thư", "Phường 14", "THPT", 2022, 12.5);
        Student student2 = new Student("Dia", "P. Bình Hưng Hòa B", "Mẫu giáo", 2021, 10);
        System.out.println(student1 + "," + student2);

        Staff staff1 = new Staff("Minh", "THĐ", "Phú Lâm", 5.5);
        Staff staff2 = new Staff("Ho", "LK4-5", "Nguyễn Đức Cảnh", 6.8);
        System.out.println(staff1 + "," + staff2);
    }
}